//Escriba un algoritmo que ponga cero en la primera y la última fila, y en la primera y la última columna de la matriz. 
public class Ejercicio3Punto2 {
    public static void main(String[]args){
        int n = 10;
        int mat[][] = new int[n][n];
    
        for( int i = 0; i < mat.length; i++ ) {
            for( int j = 0; j < mat.length; j++ ) {
                if ((i == 0 || i == n-1) || (j ==0 || j == n-1)){
                    mat[i][j] = 0;
                    
                }
                else { mat[i][j] = 1;}
                
                }
            }
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                System.out.print(mat[i][j] + " ");
                }
                System.out.println("");
                }
        }
}