/*Escriba un algoritmo que llene todas las filas pares con los números
1,2,3,...N, y las filas impares con los números N,N-1,N-2,...1*/
public class Ejercicio3Punto5 {
    public static void main(String[]args){
        int n = 10;
        int mat[][] = new int [n][n];

        for( int i = 0; i < mat.length; i++ ) {
            for( int j = 0; j < mat.length; j++ ) {
                if( (1+i)%2 == 0  ){
                    mat[i][j] = (j + 1) ;
                } else {
                    mat[i][j] = n - j ;
                }
    
            }
        }
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat.length; j++) {
                System.out.print(mat[i][j] + " ");
                }
                System.out.println("");
                }
    }
}