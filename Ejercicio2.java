import java.util.Scanner;
public class Ejercicio2 {
    public static void main(String[]args){
        int atencion[] = new int[5];
        int calidaComida[] = new int[5];;
        int justiciaDelPrecio[] = new int[5];;
        int ambiente[] = new int[5];
        double sumaAtencion = 0, sumaCalidaComida = 0, sumaJusticiaDelPrecio = 0, sumaAmbiente = 0;
        Scanner entrada = new Scanner(System.in);

        System.out.println("--Sistema de evaluacion de restaurante--");
        System.out.println("A continuacion se le pedira que califique del 1 al 10 algunos aspectos del restaurante.\n Donde 1 espesimo y 10 es excelente o inmejorable.");

        for(int i=0; i < 5; i++){ 
        System.out.println("-- ENTREVISTA A CLIENTE #"+ (i+1)+" --");
        System.out.println("---Califique los siguientes aspectos: ");
        System.out.println("1) Atencion por parte de los empleados: ");
        atencion[i] = entrada.nextInt();
        System.out.println("2) Calidad de la comida: ");
        calidaComida[i] = entrada.nextInt();
        System.out.println("3) Justicia del precio: ");
        justiciaDelPrecio[i] = entrada.nextInt();
        System.out.println("4) Ambiente");
        ambiente[i] = entrada.nextInt();
        }
        for(int i = 0; i< atencion.length; i++){
            sumaAtencion += atencion[i];
        }
        for(int i = 0; i< calidaComida.length; i++){
            sumaCalidaComida += calidaComida[i];
        }
        for(int i = 0; i< justiciaDelPrecio.length; i++){
            sumaJusticiaDelPrecio += justiciaDelPrecio[i];
        }
        for(int i = 0; i< ambiente.length; i++){
            sumaAmbiente += ambiente[i];
        }
        
        System.out.println("Promedio obtenido en el aspecto de Atencion por parte de los empleados: " + sumaAtencion/5);
        System.out.println("Promedio obtenido en el aspecto de Calidad de la comida: " + sumaCalidaComida/5);
        System.out.println("Promedio obtenido en el aspecto de Justicia del precio: " + sumaJusticiaDelPrecio/5);
        System.out.println("Promedio obtenido en el aspecto de : " + sumaAmbiente/5);
    }
}