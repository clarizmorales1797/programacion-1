import java.util.Scanner;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

public class Ejercicio4 {
    static Scanner entrada = new Scanner(System.in);
    static final JDialog dialogo = new JDialog();
    
    public static void main(String[] args) {
        int[][] A, B;
        int n = 0, m = 0, opc, n1 = 0 ,m1 = 0;
        dialogo.setAlwaysOnTop(true);
        do{
            opc = mostrarMenu();
            switch(opc){
                case 1: {
                    pedirDimensiones(n, m);

                    A = new int[n][m];
                    B = new int[n][m];

                    System.out.println("\nDigite los elementos de la primera matriz: ");
                    llenarMatriz(A);
                    System.out.println("Digite los elementos de la segunda matriz: ");
                    llenarMatriz(B);

                    System.out.println("La primera matriz ingresada es: ");
                    mostrarMatriz(A);
                    System.out.println("\nLa segunda matriz ingresada es: ");
                    mostrarMatriz(B);
                    sumarMatrices(A, B, n, m);
                }break;
                case 2: {
                    pedirDimensiones(n, m);
                    A = new int[n][m];

                    System.out.println("\nDigite los elementos de la matriz: ");
                    llenarMatriz(A);
                    System.out.println("\nLa matriz ingresada es: ");
                    mostrarMatriz(A);
                    matrizTranspuesta(A, n, m);

                }break;
                case 3: {
                    do{
                     System.out.print("Digite el orden de la matriz cuadrada: ");
                     n = entrada.nextInt();
                     if(n <= 0){
                         JOptionPane.showMessageDialog(dialogo, "Por favor, No ingrese valores negativos");
                        }
                    }while(n <= 0);

                    A = new int[n][n];

                    System.out.println("Digite los elementos de la matriz: ");
                    llenarMatriz(A);
                    matrizTraza(A);

                }break;
                case 4: {
                    System.out.println("Digite las dimensiones de la matriz A: ");
                    pedirDimensiones(n, m);
                    A = new int[n][m];
                    do{
                        System.out.println("\nDigite las dimensiones de la matriz B (La cantidad de filas de B debe ser igual a la cantidad de columnas de A): ");
                        pedirDimensiones(n1, m1);
                        if(m != n1){
                            JOptionPane.showMessageDialog(dialogo, "Las cantidad de filas de B deben ser iguales a las columnas de A");
                        }
                    }while(m != n1);
                    B = new int[n1][m1];

                    System.out.println("\nDigite los elementos de la matriz A: ");
                    llenarMatriz(A);
                    System.out.println("\nDigite los elementos de la matriz B: ");
                    llenarMatriz(B);

                    System.out.println("\nLa matriz A es: ");
                    mostrarMatriz(A);
                    System.out.println("\nLa matriz B es: ");
                    mostrarMatriz(B);
                    multiplicarMatrices(A, B);
                }break;
            }
        }while(opc != 5);

    }

    public static int mostrarMenu(){
        int opc;
        do{
            System.out.println("\n//Progama de manejo de matrices//");
            System.out.println("1.Sumar dos matrices");
            System.out.println("2.Matriz transpuesta");
            System.out.println("3.Traza de una matriz");
            System.out.println("4.Multiplicacion de dos matrices (A * B)");
            System.out.println("5.Salir");
            System.out.print("Digite una opcion: ");
            opc = entrada.nextInt();
            if(opc > 5 || opc <= 0){
                JOptionPane.showMessageDialog(dialogo, "La opcion ingresada no existe");
            }
        }while(opc <= 0 || opc > 5);

        return opc;
    }

    public static void llenarMatriz(int[][] m){
        for(int i = 0; i < m.length; i++){
            for(int j = 0; j < m[i].length; j++){
                System.out.print("Elemento [" + i + "][" + j + "]: ");
                m[i][j] = entrada.nextInt();
            }
        }
    }

    public static void sumarMatrices(int[][] a, int[][] b, int n, int m){
        int[][] C = new int[n][m];

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                C[i][j] = a[i][j] + b[i][j];
            }
        }

        System.out.println("\nLa matriz resultante de la suma es: ");
        mostrarMatriz(C);
    }

    public static void mostrarMatriz(int[][] m){
        for(int i = 0; i < m.length; i++){
            for(int j = 0; j < m[i].length; j++){
                System.out.print(m[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void matrizTranspuesta(int[][] a, int n, int m){
        int[][] A = new int[m][n];

        for(int i = 0; i < m; i++){
            for(int j = 0; j < n; j++){
                A[i][j] = a[j][i];
            }
        }

        System.out.println("\nLa matriz transpuesta es: ");
        mostrarMatriz(A);
    }
    
    public static void pedirDimensiones(int n, int m){
        do{
            System.out.print("\nDigite el numero de filas: ");
            n = entrada.nextInt();
            System.out.print("Digite el numero de columnas: ");
            m = entrada.nextInt();

            if(n <= 0 || m <= 0){
                JOptionPane.showMessageDialog(dialogo, "Por favor, No ingrese valores negativos");
            }
        }while(n <= 0 || m <= 0);

    }

    public static void matrizTraza(int[][] m){
        int traza = 0;

        for(int i = 0; i < m.length; i++){
            for(int j = 0; j < m[i].length; j++){
                if(i == j){
                    traza += m[i][j];
                }
            }
        }

        System.out.println("La traza de la matriz es: " + traza);
    }

    public static void multiplicarMatrices(int[][] a, int[][] b){
        int p = 0, suma = 0, k = 0;
        int[][] C = new int[a.length][a.length];

        for(int i = 0; i < a.length; i++){
            for(int j = 0; j < a[i].length; j++){
                suma += a[k][p] * b[p][i];
                p++;
            }
            p = 0;
            C[k][i] = suma;
        }

        k++;
        suma = 0;

        for(int i = 0; i < a.length; i++){
            for(int j = 0; j < a[i].length; j++){
                suma += a[k][p] * b[p][i];
                p++;
            }
            p = 0;
            C[k][i] = suma;
        }

        System.out.println("\nLa matriz resultante de la multiplicacion es: ");
        mostrarMatriz(C);
    }

}
