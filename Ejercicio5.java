import java.util.Arrays;
import java.util.Scanner;


public class Ejercicio5{
    public static void main(String[] args) {
        int array[] = null;
        boolean flag = true;
        int opc = 0;
        int maximo = 0;
        int minimo = 0;
        int pares = 0;
        int suma = 0;
        int[] miArreglo = new int [10];
        Scanner scan = new Scanner(System.in);
        do{
            menu();
            opc = scan.nextInt();

            switch (opc){
                case 1:
                    for(int i=0;i<10;i++){
                        System.out.print("ingrese el numero en la posicion " + (i) + ": ");
                        miArreglo[i] = scan.nextInt();
                    }
                    break;
                case 2:
                    System.out.println("Lista de Numeros: ");
                    for(int x : miArreglo){
                        System.out.print("\t" + x);
                    }
                    System.out.println();
                    break;
                case 3:
                    for (int i=0;   i<miArreglo.length; i++){
                        if (miArreglo[i] % 2 == 0){
                            System.out.println("el numero que se encuentra en la posicion: " + i + "es par {" + miArreglo[i] + "}");
                        }
                    }
                    
                    break;    
                case 4:
                    for (int i=0;i<miArreglo.length;i++){
                        if (maximo  < miArreglo[i]){
                            maximo = miArreglo[i];

                        }
                    }       
                    System.out.println("el numero maximo es: "+ maximo );
                    break;  
                case 5:
                    for(int i=0;i<miArreglo.length;i++){
                        if(minimo > miArreglo[i]){
                            minimo = miArreglo[i];
                        }
                    }      
                    System.out.println("el numero minimo: " + minimo);              
                    break;             
                case 6:
                    flag = false;
            }

        }while(flag);
        scan.close();
        
    }

    public static int[] add(int vector[], int value){
        if(vector == null){
            vector = new int[1];
            vector[0] = value;
            return vector;
        }

        int tmp[] = Arrays.copyOf(vector, vector.length+1);
        tmp[tmp.length - 1] = value;

        return tmp;
    }

    public static void menu(){
        System.out.println("1. Agregar numero");
        System.out.println("2. Ver numeros" );
        System.out.println("3. numeros pares");
        System.out.println("4. numero maximo");
        System.out.println("5. numero minimo");
        System.out.println("6. Salir");
    }
}