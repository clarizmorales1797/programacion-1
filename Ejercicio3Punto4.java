/**Escriba un algoritmo que llene la diagonal principal de la matriz con
los números 1,2,3,...N. La diagonal principal de una matriz está
formada por las casillas en las cuales el índice de fila y de columna son iguales.*/
public class Ejercicio3Punto4 {
    public static void main(String[]args){
        int n = 10;
        int mat[][] = new int [n][n];

        for( int i = 0; i < mat.length; i++ ) {
            for( int j = 0; j < mat.length; j++ ) {
                if( i == j ){
                    mat[i][j] = (i + 1);
                } else {
                    mat[i][j] = 1;
                }
    
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(mat[i][j] + " ");
                }
                System.out.println("");
                }
    }
}