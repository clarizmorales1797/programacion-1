//Escriba un algoritmo que ponga cero en ambas diagonales de la matriz.
public class Ejercicio3Punto1 {
    public static void main(String[]args){
    int n = 10;
    int mat[][] = new int[n][n];

    for( int i = 0; i < mat.length; i++ ) {
        for( int j = 0; j < mat.length; j++ ) {
            if( i == j ){
                mat[i][j] = 0;
                mat[i][n - j - 1] = 0;
                mat[n - i - 1][j] = 0;

            } else {
                mat[i][j] = 1;
            }

        }
    }
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            System.out.print(mat[i][j] + " ");
            }
            System.out.println("");
            }
    }
}
