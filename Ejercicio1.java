import java.util.Scanner;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

public class Ejercicio1{
    static final JDialog dialogo = new JDialog();

    public static void main(String[] args) {
        dialogo.setAlwaysOnTop(true);
        int[] arreglo, posiciones;
        int n, x;
        Scanner entrada = new Scanner(System.in);

        do{
        System.out.print("Ingrese el numero de elementos del arreglo: ");
        n = entrada.nextInt();
        }while(n <= 0);
        
        arreglo = new int[n];
        posiciones = new int[n];

        System.out.println("\nIngrese los elementos del arreglo: ");
        for(int i = 0; i < n; i++){
            System.out.print("Elemento ." + (i+1) + ": ");
            arreglo[i] = entrada.nextInt();
        }

        System.out.print("Ingrese el numero a buscar en el arreglo: ");
        x = entrada.nextInt();
        entrada.close();
        buscarX(arreglo, posiciones, x);

    }

    public static void buscarX(int[] v, int[] vp, int x){
        int contador = 0;
        boolean verificador = false;

        for(int i = 0; i < v.length; i++){
            if(v[i] == x){
                vp[contador] = i;
                contador++;
                verificador = true;
            }
        }

        if(verificador == false){
            JOptionPane.showMessageDialog(dialogo,"No se ha encontrado el numero " + x + " en el arreglo");
        }else{
            System.out.println("\nEl numero " + x + " se encontro en las siguiente posiciones del arreglo: ");
            mostrarArreglo(vp, contador);
        }
    }

    public static void mostrarArreglo(int[] a, int limite){
        for(int i = 0; i < limite; i++){
            System.out.print(a[i] + " ");
        }
    }
}